var newinfo;
var totalAdsBlocked = 0;
var totalTrackerBlocked = 0;

var htmlStr = {
    'container': document.querySelector('.aj-container'),
    'siteNameDiv': document.querySelectorAll('.siteurlnm'),
    'extensionStatusBtn': document.querySelectorAll('.c-btn-red'),
    'whiteListWrapper': document.querySelector('.site-wrapper'),
    'whiteListBtn': document.querySelector('.trust-this-site'),
    'adtrackCountDivs': document.querySelectorAll('.adtrackCount'),
    'insecurecookieCountDiv': document.querySelector('.insecureCookieCt'),
    'totalCookieCountDiv': document.querySelector('#totalCookiesCt'),
    'encryptedStatusDiv': document.querySelector('.encrypt-back-icon'),
    'encryptedStatusSection': document.querySelector('.encryptSection'),
    'adslistsection': document.querySelector('.adslistsection'),
    'trackerslistsection': document.querySelector('.trackerslistsection'),
    'cookiesSection': document.querySelector('.tracks_cookies'),
    'blockedAdsection': document.querySelector('.block-ads-back-icon'),
    'blockedCookieSection': document.querySelector('.cookies-back-icon')
}

$(document).ready(function() {
    $(".aj-setting-head svg, .go-to-setting").click(function() {
        // document.querySelector('.aj-setting-head svg,.go-to-setting').addEventListener('click', function(event) {
        $(".section-right-0").removeClass("section-right-0");
        $(".aj-setting-section").addClass("section-right-0");

    });
    $(".site-wrapper li").click(function() {
        if(!$(this).hasClass('li-grey')){
            $(".section-right-0").removeClass("section-right-0");
            $("." + $(this).attr("tracks-slide")).addClass("section-right-0");
        }
    });
    $(".prev-back-normal").click(function() {
        $(".section-right-0").removeClass("section-right-0");
    });
    $(".aj_blocked-details").click(function() {
        $('.aj_blocked-details').animate({
            scrollTop: 0
        });
        if ($(this).hasClass("accordian-active")) {
            $('.aj_blocked-details').animate({
                scrollTop: 0
            },1);
            $(".aj_blocked-details").removeClass("accordian-active");
        } else {   
            $('.aj_blocked-details').animate({
                scrollTop: 0
            },1);         
            $(".aj_blocked-details").removeClass("accordian-active");
            $(this).addClass("accordian-active");        
        }
        
    });

    $(".site-wrapper li").hover(
        function() {
            if($(this).hasClass('li-grey'))
                $(this).find('.toolTipDiv').show();
        },
        function(){
            $(this).find('.toolTipDiv').hide();
        }
    );

    $("#TrustSiteController").hover(
        function() {
            if($(this).parents('.site-wrapper').hasClass('trusted'))
                $(this).find('.toolTipDiv').show();
        },
        function(){
            $(this).find('.toolTipDiv').hide();
        }
    );

    $(".PauseController").hover(
        function() {
            if($(this).parents('.aj-container').hasClass('disabledExtension'))
                $(this).find('.toolTipDiv').show();
        },
        function(){
            $(this).find('.toolTipDiv').hide();
        }
    );
  setTimeout(function(){
    $(".chart-item-0").mousemove(function(e){
        $(".donut-chart-bg-tooltip_0").addClass("hover-active");
       // $(this).addClass("mousemove-active");
       //var x = e.clientX;
       //var y = e.clientY;
      // $(".hover-active").css("transform","translate3d("+x+"px,"+y+"px,0px)");
       }).mouseleave(function(){
        // $(".chart-item-0").removeClass("mousemove-active");
         $(".donut-chart-bg-tooltip_0").removeClass("hover-active");
     });
     $(".chart-item-1").mousemove(function(e){
        //$(this).addClass("mousemove-active");
         $(".donut-chart-bg-tooltip_1").addClass("hover-active");
        }).mouseleave(function(){
           // $(".chart-item-1").removeClass("mousemove-active");
          $(".donut-chart-bg-tooltip_1").removeClass("hover-active");
      });

  },1000);
   
    
});


var BG = chrome.extension.getBackgroundPage();

BG.getCurrentTabInfo(function(info) {
    // setTimeout(function(){refreshPopup() }, 500);
    newinfo = info;
    BG.console.log(info);

    //     tab:tab,                                     //details of the tab on which the pop is opened (object)
    //     total_blocked:total_blocked,                 // count of totalads blocked
    //     tab_ad_array : tab_ad_array,                 // array of ad adday(domain,count)
    //     tab_privacy_array : tab_privacy_array,       // array of tracker array(domain,count)
    //     secure_url : secure_url,                     //true if url is safe otherise false
    //     cookiesArray : cookiesArray,                 // array of all the cookies(name,domain,secureflag)
    //     whitelisted : iswhiteListed,                 // if the site i trusted of not
    //     blockingStatus : blockingEnabled,              // status if ad blocking and tracking is on or off
    //     cookieTrackingEnabled : cookieTrackingEnabled, //status if cookie tracking is on or off
    //     isDisabled : isDisabled                        //status if the extension is on or off


    // set enabled/disabled state
    var taburl = newinfo.tab.url;
    taburl = taburl.split('//')[1].split('/')[0];
    // if(newinfo.blockingStatus == false || newinfo.isDisabled == true) {
    //     newinfo.tab_ad_array=[];
    //     newinfo.tab_privacy_array=[];
    //     newinfo.total_blocked=0;
    // }
    Array.from(htmlStr.siteNameDiv).forEach(function(el) {
        el.innerText = taburl;
    })

    Array.from(htmlStr.adtrackCountDivs).forEach(function(el) {
        el.innerText = newinfo.total_blocked;
    });


    //Functions for handling various events
    extensionDisable(newinfo.isDisabled);
    pageWhiteListed(newinfo.whitelisted);

    var adCount = populateAdSection(newinfo.tab_ad_array);
    populateTrackerSection(newinfo.tab_privacy_array);
    populateCookieSection(newinfo.cookiesArray);
    secureUrlHandling(newinfo.secure_url);
    trackerSliderControl(newinfo.blockingStatus);
    cookieSliderControl(newinfo.cookieTrackingEnabled);

    document.querySelector('.aj-wrapper').style.display = 'block';
    setTimeout(function() { svg_anim(adCount) }, 100);
});

Array.from(document.getElementsByClassName("TrackerController")).forEach(function(el) {
    el.addEventListener("change", function() {
        if (BG.getStatus()) {
            BG.disable();
            trackerSliderControl(false);
        } else {
            BG.enable();
            trackerSliderControl(true);
        }
    })
})


Array.from(document.getElementsByClassName("CookieController")).forEach(function(el) {
    el.addEventListener("change", function() {
        if (BG.getCookieTrackingStatus()) {
            BG.disableCookieTracking();
            cookieSliderControl(false);
        } else {
            BG.enableCookieTracking();
            cookieSliderControl(true);
        }
    })
})

Array.from(document.getElementsByClassName("PauseController")).forEach(function(el) {
    el.addEventListener("click", function() {
        if (BG.getExtensionActiveStatus() == false) {
            BG.disableExtension();
            extensionDisable(true);
        } else {
            BG.enableExtension();
            extensionDisable(false);
        }
    })
})

// Array.from(document.getElementsByClassName("backButtonHandler")).forEach(function(el) {
//     el.addEventListener("click", function() {
//         refreshPopup();
//     })
// })

document.getElementById("TrustSiteController").addEventListener("click", function() {
    if (BG.isWhiteListed(newinfo.tab.url)) {
        pageWhiteListed(false);
        BG.removeWhiteList(newinfo.tab.url);
    } else {
        pageWhiteListed(true);
        BG.addWhiteList(newinfo.tab.url);
    }
})

function secureUrlHandling(secureUrlFlag) {
    if (secureUrlFlag == false) {
        htmlStr.encryptedStatusDiv.querySelector('.site-wrapper-icon').classList.add('activeted-cancel');
        htmlStr.encryptedStatusDiv.querySelector('.site-wrapper-title').innerText = 'Not Encrypted';
        htmlStr.encryptedStatusDiv.querySelector('.site-wrapper-text').innerText = 'This website is not secure';

        htmlStr.encryptedStatusSection.classList.add('encripted-cancel');
        htmlStr.encryptedStatusSection.querySelector('.encript-title').innerText = 'Connection Not Encrypted';
        htmlStr.encryptedStatusSection.querySelector('.encript-sub-text').innerText = 'This website is not secure';
    } else {
        htmlStr.encryptedStatusDiv.querySelector('.site-wrapper-icon').classList.remove('activeted-cancel');
        htmlStr.encryptedStatusDiv.querySelector('.site-wrapper-title').innerText = 'Encrypted';
        htmlStr.encryptedStatusDiv.querySelector('.site-wrapper-text').innerText = 'This website is secure';

        htmlStr.encryptedStatusSection.classList.remove('encripted-cancel');
        htmlStr.encryptedStatusSection.querySelector('.encript-title').innerText = 'Connection Encrypted';
        htmlStr.encryptedStatusSection.querySelector('.encript-sub-text').innerText = 'This website is encrypted and secure';
    }
}

function trackerSliderControl(blockingStatus) {
    if (blockingStatus == false) {
        Array.from(document.querySelectorAll('.TrackerController')).forEach(function(el) {
            el.removeAttribute('checked');
        })
        htmlStr.blockedAdsection.classList.add('li-grey');
    } else {
        Array.from(document.querySelectorAll('.TrackerController')).forEach(function(el) {
            el.setAttribute('checked', 'checked');
        })
        htmlStr.blockedAdsection.classList.remove('li-grey');
    }
}

function cookieSliderControl(cookieTrackingEnabled) {
    if (cookieTrackingEnabled == false) {
        Array.from(document.querySelectorAll('.CookieController')).forEach(function(el) {
            el.removeAttribute('checked');
        })
        htmlStr.blockedCookieSection.classList.add('li-grey');
    } else {
        Array.from(document.querySelectorAll('.CookieController')).forEach(function(el) {
            el.setAttribute('checked', 'checked');
        })
        htmlStr.blockedCookieSection.classList.remove('li-grey');
    }
}

function populateAdSection(tab_ad_array) {
    var adslistsection = htmlStr.adslistsection;
    var adslistcont = adslistsection.querySelector('.details-accordian');
    var count = 0;
    tab_ad_array.forEach(function(key, value, map) {
        var childel = document.createElement('div');
        childel.className = 'accordian-child';
        childel.innerHTML = '<div class="block_ads_count">' + key + '</div><div class="block_ads_webname">' + getLocation(value).hostname.split('.').slice(-2).join('.') + '</div><div class="block_ads_weburl">' + value + '</div>';
        adslistcont.appendChild(childel);
        count += key;
        totalAdsBlocked += parseInt(key);
    });

    adslistsection.querySelector('.ads-block-count').innerText = totalAdsBlocked;
    document.querySelector('.tooltip_Blocked_ADs').querySelector('.tooltip_num').innerText = totalAdsBlocked;

    return count;

}

function populateTrackerSection(tab_privacy_array) {
    // BG.console.log(tab_privacy_array);
    var trackerslistsection = htmlStr.trackerslistsection;
    var trackerlistcont = trackerslistsection.querySelector('.details-accordian');
    tab_privacy_array.forEach(function(key, value, map) {
        var childel = document.createElement('div');
        childel.className = 'accordian-child';
        childel.innerHTML = '<div class="block_ads_count">' + key + '</div><div class="block_ads_webname">' + getLocation(value).hostname.split('.').slice(-2).join('.') + '</div><div class="block_ads_weburl">' + value + '</div>';
        trackerlistcont.appendChild(childel);
        totalTrackerBlocked += parseInt(key);
    });

    trackerslistsection.querySelector('.ads-block-count').innerText = totalTrackerBlocked;
    document.querySelector('.tooltip_Trackers').querySelector('.tooltip_num').innerText = totalTrackerBlocked;
}

function populateCookieSection(cookiesArray) {
    // BG.console.log(cookiesArray);
    var cookieCount = cookiesArray.length;
    var insecureCookiesCount = Array.from(cookiesArray).reduce(function(count, el) {
        if (el.secure == false)
            return count + 1;
        else
            return count;
    }, 0)
    htmlStr.totalCookieCountDiv.innerText = cookieCount;
    htmlStr.insecurecookieCountDiv.innerText = insecureCookiesCount;

    htmlStr.cookiesSection.querySelector('.insecure_cookies').querySelector('.ads-block-count').innerText = insecureCookiesCount;
    htmlStr.cookiesSection.querySelector('.secure_cookies').querySelector('.ads-block-count').innerText = cookieCount - insecureCookiesCount;

    newinfo.cookiesArray.forEach(function(obj) {
        var childel = document.createElement('div');
        childel.className = 'accordian-child';
        childel.innerHTML = '<div class="block_ads_webname"><span class="spacer">Name:</span>' + obj.name  + "@" + obj.domain + '</div><div class="block_ads_weburl"><span class="spacer">Value:</span>' + obj.value + '</div>';
        if (obj.secure)
            htmlStr.cookiesSection.querySelector('.secure_cookies').querySelector('.details-accordian').appendChild(childel);
        else
            htmlStr.cookiesSection.querySelector('.insecure_cookies').querySelector('.details-accordian').appendChild(childel);
    });
}

function extensionDisable(isDisabled) {
    if (isDisabled) {
        htmlStr.container.classList.add('disabledExtension');
        Array.from(htmlStr.extensionStatusBtn).forEach(function(el){
            el.querySelector('.btnText2').innerText = 'Resume Extension';
        })
        htmlStr.whiteListWrapper.classList.add('li-grey');
    } else {
        htmlStr.container.classList.remove('disabledExtension');
        Array.from(htmlStr.extensionStatusBtn).forEach(function(el){
            el.querySelector('.btnText2').innerText = 'Pause Extension';
        })
        htmlStr.whiteListWrapper.classList.remove('li-grey');
    }
}

function pageWhiteListed(whitelisted) {
    if (whitelisted) {
        htmlStr.whiteListWrapper.classList.add('trusted');
        htmlStr.whiteListBtn.querySelector('.btnText').innerText = 'Trusted Site';
    } else {
        htmlStr.whiteListWrapper.classList.remove('trusted');
        htmlStr.whiteListBtn.querySelector('.btnText').innerText = 'Trust This Site';
    }
}



function refreshPopup() {
    window.location.href = "popup.html";
}

var getLocation = function(href) {
    var l = document.createElement("a");
    l.href = href;
    return l;
};


function svg_anim(count){
    //BG.console.log(svg_ads_block);
    //alert(svg_ads_block);
    function DonutChart(parent, spec) {
        var __polar2xy = function(a, r) {
        return {
            x:  Math.cos(a * 2 * Math.PI) * r,
            y: -Math.sin(a * 2 * Math.PI) * r,
        }
        }
    
        var __gen_arc_path = function(cx, cy, r, start, offset) {
        var end = __polar2xy(start + offset, r)
        start = __polar2xy(start, r)
        return [
            "M", cx + start.x, cy + start.y,
            "A", r, r, 0, +(offset > .5), 0, cx + end.x, cy + end.y,
        ].join(" ")
        }
    
        var __gen_chart_item = function(out, c, r, prev, cur, i, stroke) {
        out.push(["path", {
            d: __gen_arc_path(c, c, r, prev, cur),
            class: "chart-item-" + i,
            fill: "transparent",
            "stroke-width": stroke
           // onmouseenter:"svg_mouseenter('donut-chart-bg-tooltip_"+i+"')",
           // onmouseleave:"svg_mouseleave('donut-chart-bg-tooltip_"+i+"')"
        }])
        }
    
        var __gen_chart = function(chart) {
        var prev = 0, out = []
        // FIXME get radius and stroke-width from CSS
        var c = chart.r, r = chart.r - chart.stroke / 2
        for (var i in chart.items) {
            cur = chart.items[i]
            __gen_chart_item(out, c, r, prev, cur.value, i, chart.stroke)
            prev += cur.value
        }
        if (prev < 1) {
            __gen_chart_item(out, c, r, prev, 1 - prev, "bg", chart.stroke)
        }
        return out
        }
    
        var __create_tag_tree = function(elem) {
        var root = document.createElementNS("http://www.w3.org/2000/svg", elem[0])
        var attr = elem[1]
        // Set attributes
        for (var i in attr) {
            var a = document.createAttribute(i)
            a.value = attr[i]
            root.setAttributeNode(a)
        }
        // Create children nodes
        if (elem.length > 2) {
            var children = elem[2]
            for (var i in children) {
            var c = __create_tag_tree(children[i])
            root.appendChild(c)
            }
        }
        return root
        }
    
    
        /* Transformation matrix (rotate and mirror) to correct orientation:
         * \[
         *   \left[
         *   \begin{array}{ccc}
         *      0 & -1 & 0 \\
         *     -1 &  0 & 0 \\
         *      0 &  0 & 1
         *   \end{array}
         *   \right]
         * \]
         */
        var correct_orientation = "matrix(0 -1 -1 0 0 0)"
    
        var __gen_code = function(spec) {
        return __create_tag_tree(
            ["svg", {
            transform: correct_orientation,
            class: "chart-donut",
            // onmouseenter:"svg_mouseenter('donut-chart-tooltip')",
            // onmouseleave:"svg_mouseleave('donut-chart-tooltip')",
            width: spec.r * 2,
            height: spec.r * 2,
            }, __gen_chart(spec)])
        }
    
        var __is_dict = function(v) {
        return v && typeof v === "object" && !(v instanceof Array)
        }
    
        DonutChart.prototype.update = function(spec) {
        // Merge the new spec
        for (var i in spec) {
            this.spec[i] = spec[i]
        }
        var code = __gen_code(this.spec)
        // TODO can we switch the elements in place?
        if (this.element != undefined) {
            this.element.remove()
        }
        this.element = this.parent.appendChild(code)
        }
        this.parent = parent
        this.spec = spec       
        this.update({})
    }
    /*svg create start*/
    var svg_ads_value = count;
    //alert(count);
    var svg_total_block = parseInt($(".adsblock_count").text());
    var svg_ads_block = parseInt(svg_ads_value / svg_total_block * 100);
    var chart_div = document.getElementById("blokerchart")
    if(count>0){      
    var mychart_1 = new DonutChart(chart_div, {
        r: 60,
        stroke: 16,
        scale: 100,
        items: [
            { label: "A", value: svg_ads_block/100 },
            { label: "B", value: (100-svg_ads_block)/100 },
        ]
    });

    setTimeout(function(){
            $(".chart-donut").css('stroke-dashoffset', 0);
    },200);
    }
    
}

/*svg create End*/